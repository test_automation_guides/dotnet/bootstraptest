﻿using Coypu.Drivers;
using Coypu.Drivers.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;

namespace BootStrapTest
{
	public class BSChromeDriver : SeleniumWebDriver
    {
        private static ChromeDriver chromedriver;

        public BSChromeDriver(int width, int height, bool headless = false)
            : base(CustomProfileDriver(width, height, headless), Browser.Chrome)
        {
        }

        public new void Dispose()
        {
            chromedriver.Dispose();
            base.Dispose();
        }

        private static RemoteWebDriver CustomProfileDriver(int width= 1920, int height = 1080, bool headless = false)
        {
            ChromeOptions options = new ChromeOptions();
            if (headless == true) options.AddArgument("--headless");

            options.AddArgument($"--window-size={width},{height}");
            options.AddArgument("--no-sandbox");
            options.AddArgument("--disable-dev-shm-usage");
            chromedriver = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory, options);

            return chromedriver;
        }

    }
}
