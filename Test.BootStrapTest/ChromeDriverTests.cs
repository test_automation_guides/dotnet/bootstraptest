using BootStrapTest;
using Coypu;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.BootStrapTest
{
	[TestClass]
	public class ChromeDriverTests
	{
		[TestMethod]
		public void CanCreateChromeBrowser()
		{
			var driver = new BSChromeDriver(1080, 640, true);
			var browser = new BrowserSession(new SessionConfiguration() { 
				Browser = Coypu.Drivers.Browser.Chrome
			}, driver);
			browser.Location.AbsoluteUri.Should().Contain("data:");
			browser.Dispose();
		}
	}
}
