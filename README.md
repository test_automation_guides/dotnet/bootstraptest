# BootStrapTest

Idea is to make it fast to get going performing UI and API testing in C#.

## Dependencies

Ensure that you have the nuget package `Selenium.WebDriver.ChromeDriver` installed as the way a browser is created uses it's driver.

## Create a Chrome browser

Example below shows how to create a Chrome browser with dimensions width 1080 and height 640px.

```csharp
var driver = new BSChromeDriver(1080, 640);
var browser = new BrowserSession(new SessionConfiguration() { 
	Browser = Coypu.Drivers.Browser.Chrome
}, driver);
```
